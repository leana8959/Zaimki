export default {
    'support': [2557549454, 'auto', true],

    'aside-left-top': null,
    'aside-left-middle': [4999879969, 'auto', true],
    'aside-left-bottom': null,

    'aside-right-top': null,
    'aside-right-middle': [2369847195, 'auto', true],
    'aside-right-bottom': [6664353412, 'auto', true],

    'main-0': [8172838213, 'auto', true],
    'main-1': [3299823474, 'auto', true],
    'main-2': [3108251782, 'auto', true],
    'main-3': [6716873048, 'auto', true],
    'main-4': null,
    'main-5': null,
    'main-6': null,

    'footer': [6584462360, 'autorelaxed', false],

    'small-homepage': [6146027401, 'auto', true],
};
